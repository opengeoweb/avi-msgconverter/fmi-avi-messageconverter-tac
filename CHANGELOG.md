# Changelog - fmi-avi-messageconverter-tac

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- ...

### Changed

- ...

### Deprecated

- ...

### Removed

- ...

### Fixed

- ...

### Security

- ...

## [v4.0.0] - 2014-04-13

### Added

- Created overview documentation for developers. [#95]

### Changed

- Adapted TAF TAC support to model changes for IWXXM 3. [#97]
- Adapted to `AviationWeatherMessage.getReportStatus()` being non-`Optional`. [#90]
- Code quality enhancements. [#101]

## Past Changelog

Previous changelog entries are available on [GitHub releases page](https://github.com/fmidev/fmi-avi-messageconverter-tac/releases) in a more freeform format.

[Unreleased]: https://github.com/fmidev/fmi-avi-messageconverter-tac/compare/fmi-avi-messageconverter-tac-4.0.0...HEAD

[v4.0.0]: https://github.com/fmidev/fmi-avi-messageconverter-tac/releases/tag/fmi-avi-messageconverter-tac-4.0.0

[#90]:https://github.com/fmidev/fmi-avi-messageconverter-tac/issues/90

[#95]:https://github.com/fmidev/fmi-avi-messageconverter-tac/issues/95

[#97]:https://github.com/fmidev/fmi-avi-messageconverter-tac/issues/97

[#101]:https://github.com/fmidev/fmi-avi-messageconverter-tac/issues/101
